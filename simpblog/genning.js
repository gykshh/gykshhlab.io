"use strict";

var {fs, crypto} = new Proxy({}, {get: (_, x) => require(x)});

var baseurl = "https://gykshh.gitlab.io";

var file_code_to_lns = (x) => fs
	.readFileSync("." + x, "UTF-8")
	.replace(/^\u{FEFF}/su, "")
	.split(/\r\n?|\n/gsu)
	.filter((ln) => ln && !ln.startsWith("#"))
;

var file_post_to_html = (...x) => [...(function* recur(file, seg_id){
	var lns = file_code_to_lns(file + ".post");
	seg_id = seg_id == "*" ? null : seg_id;
	if(seg_id){
		for(
			; lns.length && lns[0] != "start " + seg_id
			; lns.shift()
		);
		lns.shift();
	}
	for(
		; lns.length && !(seg_id && lns[0] == "stop " + seg_id)
		; lns.shift()
	){
		if(lns[0].startsWith("\t"))yield lns[0].slice(1);
		if(lns[0].startsWith("quote ")){
			var [, quoted_seg_id, quoted_file] = lns[0].match(/ (\S+) (.+)/su);
			yield* recur(quoted_file, quoted_seg_id);
		}
		if(lns[0].startsWith("list tit "))yield* file_ents_to_ents(lns[0].slice(9)).map(({url, tit}) =>
			`<li><a href="` + url + `">` + tit + `</a></li>`);
		if(lns[0].startsWith("list tit+main "))yield* file_ents_to_ents(lns[0].slice(14))
			.map(({url, tit, main}) => [
				`<section>`,
				`<h1>`,
				`<a href="` + url + `">` + tit + `</a>`,
				`</h1>`,
				main,
				`</section>`,
			])
			.flat()
		;
	}
})(...x)].join("\n");

var xmlescing = x => x
	.replace(/&/gsu, "&amp;")
	.replace(/</gsu, "&lt;")
;

var file_ents_to_ents = function recur(file, wildcards = [""]){
	try{
		return file_code_to_lns(file + ".ents")
			.map((x) => x.match(/(?:(\d*) )?(.+)/su))
			.filter(([, flag]) =>
				wildcards.some((x) =>
					[...x].every((bit, cur) =>
						bit == "0" || flag?.[cur] == "1")))
			.map(([,, file]) => ({
				url: encodeURI(file + ".html"),
				...Object.fromEntries([
					"tit",
					"intro",
					"main",
				].map((x) => [x, file_post_to_html(file, x)])),
			}))
		;
	}catch(_){}
	var [file, ...wildcards] = file_code_to_lns(file + ".entsflt");
	return recur(file + ".ents", wildcards.length ? wildcards : undefined);
};

(function recur(dir){
	try{
		for(var file of fs.readdirSync(dir)){
			file = dir + file;
			if(fs.lstatSync(file).isDirectory()){
				recur(file + "/");
				continue;
			}
			[file] = file.match(/.*(?=\.[^.]*$)|.*/su);
			for(var [ext, compiling] of Object.entries({
				html: (file) =>
					`<!DOCTYPE html>\n` +
					`<html>\n` +
					`\n` +
					`<head>\n` +
					`\t<meta charset="UTF-8">\n` +
					`\t<title>` + file_post_to_html(file, "tit") + `</title>\n` +
					`\t<script type="module">\n` +
					`\t\timport("/base.mjs");\n` +
					`\t</script>\n` +
					`\t<style>\n` +
					`\t\t@import url("/base.css");\n` +
					`\t</style>\n` +
					`</head>\n` +
					`\n` +
					`<body>\n` +
					file_post_to_html(file) + `\n` +
					`</body>\n` +
					`\n` +
					`</html>`,
				atom: (file) =>
					`<?xml version="1.0"?>` +
					`<feed xmlns="http://www.w3.org/2005/Atom">` +
					file_ents_to_ents(file)
						.map(({url, tit, intro}) => {
							var code =
								`<link href="` + baseurl + url + `"/>` +
								`<title type="html">` +
								xmlescing(tit) +
								`</title>` +
								`<summary type="html">` +
								xmlescing(`<base href="` + baseurl +`/">` + intro) +
								`</summary>`;
							return `<entry>` +
								`<id>` + crypto
									.createHash("sha224")
									.update(JSON.stringify({code, intro, tit, url}))
									.digest("base64")
									.slice(0, -2)
								+ `</id>` +
								code +
								`</entry>`;
						})
						.join("")
					+
					`</feed>`,
			})){
				var dstn = file + "." + ext;
				if(fs.existsSync(dstn))continue;
				try{
					fs.writeFileSync(dstn, compiling(file.slice(1)));
				}catch(_){}
			}
		}
	}catch(_){}
})("./");