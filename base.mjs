var lnwrap = document.createElement("button");
lnwrap.textContent = "wrap lines";
lnwrap.style.display = "block";
lnwrap.style.margin = "auto";
lnwrap.addEventListener("click", () => {
	document.body.style.width = String(document.documentElement.clientWidth) + "px";
});
document.body.insertBefore(lnwrap, document.body.firstChild);
document.body.style.width = Math.min(768, String(document.documentElement.clientWidth)) + "px";